#!/usr/bin/python
import sys
import re


class OutOfScopeNumber(ValueError):
    pass


class InvalidRomanNumeralError(ValueError):
    pass


class RomanNumerals:
    numerals_dict = {
        0: ('I', 'V'),
        1: ('X', 'L'),
        2: ('C', 'D'),
        3: ('M', ''),
    }

    def __init__(self):
        self.reverse_numerals_dict = self._set_symbols_dict()
        self.max_power = max(self.numerals_dict)

    def _set_symbols_dict(self):
        symbols_dict = {}
        for k in sorted(self.numerals_dict.keys()):
            for pos, val in enumerate(self.numerals_dict[k]):
                if val:
                    val_dict = {
                        'power': k,
                        'role': pos,
                    }
                    symbols_dict[val] = val_dict
        return symbols_dict

    def calculate_power_group(self, roman_numeral_str: str):
        try:
            power = self.reverse_numerals_dict[roman_numeral_str[0]]['power']
        except KeyError as e:
            raise e
        else:
            if power < self.max_power:
                one_symbol = self.numerals_dict[power][0]
                five_symbol = self.numerals_dict[power][1]
                ten_symbol = self.numerals_dict[power+1][0]
                patterns = {
                    rf'^({one_symbol}{five_symbol})(.*)': 4,
                    rf'^({one_symbol}{ten_symbol})(.*)': 9,
                    rf'^((?={five_symbol}){five_symbol}{one_symbol}{{,3}}|{one_symbol}{{1,3}})(.*)': 1,
                }
                for pattern, val in patterns.items():
                    match = re.search(pattern, roman_numeral_str)
                    if match:
                        if val != 1:
                            count = val
                        else:
                            len_symbols = len(match.group(1))
                            if match.group(1)[0] == five_symbol:
                                count = 4 + len_symbols
                            else:
                                count = len_symbols
                        return count * 10 ** power, power, match.group(2)
                else:
                    raise ValueError(f'{roman_numeral_str} cannot be interpreted as a Roman Numeral')
            else:
                one_symbol = self.numerals_dict[power][0]
                pattern = rf'^({one_symbol}{{1,9}})([^{one_symbol}].*)'
                match = re.search(pattern, roman_numeral_str)
                if match:
                    return len(match.group(1)) * 10 ** power, power, match.group(2)
                else:
                    raise ValueError(f'{roman_numeral_str} cannot be interpreted as a Roman Numeral')

    def convert_from_roman_numeral(self, roman_numeral_str):
        try:
            value, power, remainder_str = self.calculate_power_group(roman_numeral_str)
            while remainder_str:
                last_power = power
                last_value = value
                value, power, remainder_str = self.calculate_power_group(remainder_str)
                if not power < last_power:
                    raise InvalidRomanNumeralError(f'{roman_numeral_str} cannot be interpreted as a Roman Numeral')
                value += last_value
        except ValueError as e:
            raise InvalidRomanNumeralError(str(e))
        else:
            return value

    def calculate_digit(self, digit: int, power: int):
        if not 0 <= digit <= 9:
            raise ValueError(f'{digit} is not a single digit')
        if power > self.max_power:
            raise ValueError(f'{power} is higher than the max allowed power of ten ({self.max_power}).')
        try:
            one_symb, five_symb = self.numerals_dict[power]
        except KeyError as e:
            raise e
        else:
            if power < self.max_power:
                ten_symb = self.numerals_dict[power+1][0]
                if digit == 0:
                    return ''
                elif digit < 4:
                    return one_symb * digit
                elif digit == 4:
                    return one_symb + five_symb
                elif digit < 9:
                    return five_symb + one_symb * (digit - 5)
                elif digit == 9:
                    return one_symb + ten_symb
            else:
                return one_symb * digit

    def convert_to_roman_numerals(self, number: int):
        if number < 0:
            raise OutOfScopeNumber('number should be a positive integer.')
        number_str = str(number)
        power = len(number_str) - 1
        roman_numerals_str = ''
        try:
            for i, digit in enumerate(number_str):
                roman_numerals_str += self.calculate_digit(int(digit), power - i)
        except ValueError as e:
            raise OutOfScopeNumber(str(e))
        else:
            return roman_numerals_str


def main(argv):
    if not argv:
        print("Please provide at least an integer to convert.")
        return

    rn = RomanNumerals()
    for value in argv:
        try:
            if value.isdecimal():
                print(rn.convert_to_roman_numerals(int(value)))
            else:
                print(rn.convert_from_roman_numeral(value))
        except OutOfScopeNumber as e:
            print(str(e))
        except InvalidRomanNumeralError as e:
            print(str(e))


if __name__ == "__main__":
    main(sys.argv[1:])
