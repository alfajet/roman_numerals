import pytest
from roman_numerals import RomanNumerals, OutOfScopeNumber


class TestConvertToRomanNumerals:
    def test_calculate_digits_valid(self):
        rn = RomanNumerals()
        assert rn.calculate_digit(0, 0) == ''
        assert rn.calculate_digit(1, 0) == 'I'
        assert rn.calculate_digit(3, 0) == 'III'
        assert rn.calculate_digit(4, 0) == 'IV'
        assert rn.calculate_digit(5, 0) == 'V'
        assert rn.calculate_digit(7, 0) == 'VII'
        assert rn.calculate_digit(9, 0) == 'IX'

        assert rn.calculate_digit(1, 1) == 'X'
        assert rn.calculate_digit(5, 1) == 'L'
        assert rn.calculate_digit(9, 1) == 'XC'

        assert rn.calculate_digit(1, 2) == 'C'
        assert rn.calculate_digit(5, 2) == 'D'
        assert rn.calculate_digit(9, 2) == 'CM'

        assert rn.calculate_digit(1, 3) == 'M'
        assert rn.calculate_digit(5, 3) == 'M' * 5
        assert rn.calculate_digit(9, 3) == 'M' * 9

    def test_calculate_digit_power_not_valid(self):
        rn = RomanNumerals()
        pytest.raises(ValueError, rn.calculate_digit, digit=1, power=8)
        pytest.raises(ValueError, rn.calculate_digit, digit=12, power=1)

    def test_valid_number(self):
        rn = RomanNumerals()
        assert rn.convert_to_roman_numerals(900) == 'CM'
        assert rn.convert_to_roman_numerals(249) == 'CCXLIX'
        assert rn.convert_to_roman_numerals(2012) == 'MMXII'
        assert rn.convert_to_roman_numerals(1983) == 'MCMLXXXIII'

    def test_invalid_number(self):
        rn = RomanNumerals()
        pytest.raises(ValueError, rn.convert_to_roman_numerals, number=-14)
        pytest.raises(OutOfScopeNumber, rn.convert_to_roman_numerals, number=13000)


class TestCalculateRomanNumerals:
    def test_single_numeral_valid(self):
        rn = RomanNumerals()
        assert rn.convert_from_roman_numeral('I') == 1
        assert rn.convert_from_roman_numeral('V') == 5
        assert rn.convert_from_roman_numeral('L') == 50
        assert rn.convert_from_roman_numeral('C') == 100

    def test_single_power_valid(self):
        rn = RomanNumerals()
        assert rn.convert_from_roman_numeral('IV') == 4
        assert rn.convert_from_roman_numeral('CM') == 900
        assert rn.convert_from_roman_numeral('LXX') == 70

    def test_complex_numeral_valid(self):
        rn = RomanNumerals()
        assert rn.convert_from_roman_numeral('XIV') == 14
        assert rn.convert_from_roman_numeral('MMCM') == 2900
        assert rn.convert_from_roman_numeral('MIX') == 1009
        assert rn.convert_from_roman_numeral('LXXIX') == 79

    def test_invalid_numerals(self):
        rn = RomanNumerals()
        pytest.raises(ValueError, rn.convert_from_roman_numeral, 'XM')
